package diecofer.uniovi.es.buscadorpoi;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.ShareActionProvider;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;

/**
 * Created by Diecofer on 20/05/2017.
 */

public class SettingsActivity extends AppCompatActivity {
    SeekBar sb;
    TextView tvseekbar;
    Switch sw;
    Switch swsitiosabiertos;
    TextView tvRadiob;
    TextView info;
    Button bguardar;
    public static final String PREFERENCES = "preferences";
    public static final String DISTANCIA = "distancia";
    public static final String RADIO = "radio";
    private SharedPreferences prefs = null;
    public static final String SITIOSABIERTOS ="sitios";

    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings);
        sw = (Switch) findViewById(R.id.swproximidad);
        tvseekbar=(TextView)findViewById(R.id.tvradiobusqueda);
        tvRadiob=(TextView)findViewById(R.id.tvRadioBusquedaInfo);
        info=(TextView)findViewById(R.id.tvinfosett);
        sb=(SeekBar)findViewById(R.id.sbradio);
        swsitiosabiertos=(Switch)findViewById(R.id.swsitiosabiertos);


        prefs = this.getSharedPreferences(PREFERENCES, Context.MODE_PRIVATE);
        final SharedPreferences.Editor prefsEditor = prefs.edit();

        // TODO: Colocamos los valores de las preferencias en la pantalla de settings
        prefs = getSharedPreferences(PREFERENCES,Context.MODE_PRIVATE);
        String radio = prefs.getString(RADIO,null);
        Boolean swcheck = prefs.getBoolean(DISTANCIA,false);
        Boolean swchecksitiosabiertos = prefs.getBoolean(SITIOSABIERTOS,false);
        sw.setChecked(swcheck);
        swsitiosabiertos.setChecked(swchecksitiosabiertos);
        sb.setProgress(Integer.parseInt(radio));
        if (!sw.isChecked()){
            info.setVisibility(View.INVISIBLE);
            sb.setVisibility(View.VISIBLE);
            tvseekbar.setVisibility(View.VISIBLE);
            tvRadiob.setVisibility(View.VISIBLE);
        }else{
            info.setVisibility(View.VISIBLE);
            sb.setVisibility(View.INVISIBLE);
            tvseekbar.setVisibility(View.INVISIBLE);
            tvRadiob.setVisibility(View.INVISIBLE);
        }
        sw.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    sb.setVisibility(View.INVISIBLE);
                    tvseekbar.setVisibility(View.INVISIBLE);
                    tvRadiob.setVisibility(View.INVISIBLE);
                    info.setVisibility(View.VISIBLE);

                }
                else{
                    sb.setVisibility(View.VISIBLE);
                    tvseekbar.setVisibility(View.VISIBLE);
                    tvRadiob.setVisibility(View.VISIBLE);
                    info.setVisibility(View.INVISIBLE);
                }
            }
        });

        tvseekbar.setText(String.valueOf(sb.getProgress())+" metros");
        sb.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                tvseekbar.setText(String.valueOf(progress)+" metros");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {


            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        bguardar = (Button)findViewById(R.id.bguardarsettings);
        bguardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                prefsEditor.putBoolean(DISTANCIA,sw.isChecked());
                prefsEditor.putString(RADIO,String.valueOf(sb.getProgress()));
                prefsEditor.putBoolean(SITIOSABIERTOS,swsitiosabiertos.isChecked());
                prefsEditor.commit();
                Intent intent = new Intent(getApplicationContext(),MainActivity.class);
                startActivity(intent);
            }
        });


    }
}
