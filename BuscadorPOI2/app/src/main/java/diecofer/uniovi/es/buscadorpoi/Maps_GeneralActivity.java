package diecofer.uniovi.es.buscadorpoi;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

/**
 * Created by Diecofer on 19/05/2017.
 */

public class Maps_GeneralActivity  extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    public static final String longitud = "longitud";
    public static final String latitud = "latitud";
    public static final String name = "nombre";
    public static final String lPois = "ListaPois";
    public static final String latmovil = "33.33";
    public static final String longmovil ="3.33";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }



    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        Intent intent = getIntent();
        ArrayList<POI> listaPOIs = intent.getParcelableArrayListExtra(lPois);
        Double dlatmovil=intent.getDoubleExtra(latmovil,33.33);
        Double dlongmovil=intent.getDoubleExtra(longmovil,3.33);
        LatLng home = new LatLng(33,33);
        //Añadimos todos los POIs al mapa
        for (int i=0;i<listaPOIs.size();i++){
            home = new LatLng(Double.parseDouble(listaPOIs.get(i).getLatitud()),Double.parseDouble(listaPOIs.get(i).getLongitud()));
            mMap.addMarker(new MarkerOptions().position(home).title(listaPOIs.get(i).getmName()));
        }
        // Añadimos nuestra posicion al mapa con un marcador personalizado
        LatLng movil = new LatLng(dlatmovil,dlongmovil);
        MarkerOptions marker = new MarkerOptions().position(movil).title("Tu posición");
        marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.marker));
        mMap.addMarker(marker);

        //Centramos la camara en nosotros
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(movil,15));

    }
}
