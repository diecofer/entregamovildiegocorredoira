package diecofer.uniovi.es.buscadorpoi;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

public class POI implements Parcelable, Serializable {
	
	private String mName;
	private String rating;
	private String latitud;
	private String longitud;
   	private String opennow;

	
	public POI(String name, String rating , String lat , String lng , String on ) {
		
		if (name == null  || name.isEmpty() ){
			throw new IllegalArgumentException();
		}
		
		mName = name;
		this.rating=rating;
		this.latitud=lat;
		this.longitud=lng;
        this.opennow=on;
	}

	public String getName() {
		
		return mName;
	}


	public String getRating() {
		return rating;
	}

	public void setRating(String rating) {
		this.rating = rating;
	}

	public POI(Parcel parcel) {
			readFromParcel(parcel);
		}

	public String getmName() {
		return mName;
	}

	public void setmName(String mName) {
		this.mName = mName;
	}


	public String getLatitud() {
		return latitud;
	}

	public void setLatitud(String latitud) {
		this.latitud = latitud;
	}

	public String getLongitud() {
		return longitud;
	}

	public void setLongitud(String longitud) {
		this.longitud = longitud;
	}

	public String getOpennow() {
		return opennow;
	}

	public void setOpennow(String opennow) {
		this.opennow = opennow;
	}

	@Override
		public int describeContents() {
			return 0;
		}

		@Override
		public void writeToParcel(Parcel dest, int flags) {
			dest.writeString(mName);
			dest.writeString(longitud);
			dest.writeString(latitud);
			dest.writeString(opennow);
		}

	private void readFromParcel(Parcel parcel) {
		mName = parcel.readString();
		longitud=parcel.readString();
		latitud=parcel.readString();
		opennow= parcel.readString();

	}

	public static final Parcelable.Creator<POI> CREATOR =
			new Parcelable.Creator<POI>() {

				public POI createFromParcel(Parcel in) {
					return new POI(in);
				}

				public POI[] newArray(int size) {
					return new POI[size];
				}
			};
}
