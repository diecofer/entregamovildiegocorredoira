package diecofer.uniovi.es.buscadorpoi;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;


import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;

public class POIListFragment extends Fragment implements
        AdapterView.OnItemClickListener {
    private POIAdapter mAdapter = null;
    Callbacks mCallback;
    private ArrayList<POI> listaPOIs = new ArrayList<POI>();
    public static final String LISTAPOIS = "ListaBundle";
    public static String TIPO = "museum";
    public  static double latmovil= 33.33;
    public static double longmovil = 3.33;
    public static final String PREFERENCES = "preferences";
    public static final String DISTANCIA = "distancia";
    public static final String RADIO = "radio";
    private SharedPreferences prefs = null;
    public static String radio ="500";
    public static Boolean swcheck = false;
    public static Boolean swsitios = false;
    public ProgressBar pbar;
    public TextView tvnodata;


    public interface Callbacks {
        public void onPOISelected(POI POI);
    }
    public static POIListFragment newInstance(String Tipo,Double lat, Double longi, String rad , Boolean sw, Boolean swsit) {

        POIListFragment fragment = new POIListFragment();
        TIPO=Tipo;
        latmovil=lat;
        longmovil=longi;
        radio=rad;
        swcheck=sw;
        swsitios=swsit;
        return fragment;
    }
    public POIListFragment() {
    }

    @Override
    public void onAttach(Activity activity) {

        super.onAttach(activity);
        try {
            mCallback = (Callbacks) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() +
                    " must implement Callbacks");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView;
        rootView = inflater.inflate(R.layout.poi_list_fragment,
                container, false);
        pbar=(ProgressBar)rootView.findViewById(R.id.pbjson);
        tvnodata=(TextView)rootView.findViewById(R.id.tvnoresultados);
        createPOIList(TIPO,latmovil,longmovil,radio,swcheck,swsitios);


        ListView lvItems = (ListView) rootView.findViewById(R.id.list_view_courses);

        mAdapter = new POIAdapter(this.getActivity(),listaPOIs);

           lvItems.setAdapter(mAdapter);
           // ListView lv= (ListView)rootView.findViewById(R.id.list_view_courses);
           lvItems.setOnItemClickListener(this);
        Button bmapa = (Button)rootView.findViewById(R.id.bmapa);
        bmapa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(),Maps_GeneralActivity.class);
                intent.putExtra(Maps_GeneralActivity.longmovil,longmovil);
                intent.putExtra(Maps_GeneralActivity.latmovil,latmovil);
                intent.putParcelableArrayListExtra(Maps_GeneralActivity.lPois, listaPOIs);
                startActivity(intent);
            }
        });

        return rootView;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putParcelableArrayList(LISTAPOIS,listaPOIs);
    }

    private void createPOIList(String tipo, Double lat , Double longi , String rad , Boolean swch , final Boolean swsitios) {
        String latmov = lat.toString();
        String longmov = longi.toString();
        String URLprov="";

        //Construimos la url en funcion a las preferencias. Depende de si el usuario quiere usar o no el radio
        if (swch){
              URLprov = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location="+latmov+","+longmov+"&rankby=distance&types="+TIPO+
                    "&key=AIzaSyATQxKxaXrXlbLvkmDYURzA8DgVR8neUSw";
        }else{
              URLprov = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location="+latmov+","+longmov+"&radius="+rad+"&types="+TIPO+
                    "&key=AIzaSyATQxKxaXrXlbLvkmDYURzA8DgVR8neUSw";

        }
        final String URL = URLprov;

        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,
                URL, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray datos = response.getJSONArray("results");
                    String texto = "";
                    for (int i = 0; i < datos.length(); i++) {
                        Log.i("CREATE LISTA POIS", datos.getJSONObject(i).getString("name"));


                        JSONObject pos = datos.getJSONObject(i).getJSONObject("geometry").getJSONObject("location");
                        String lat = pos.getString("lat");
                        String lng = pos.getString("lng");
                        Log.i("LATITUD", lat);
                        Log.i("LONGITUD", lng);
                        String abiertoahora = "";
                        POI nuevo = null;
                        //Comprobamos que la respuesta tenga el campo rating
                        if (!datos.getJSONObject(i).has("rating")) {
                           nuevo= new POI(datos.getJSONObject(i).getString("name")
                                    , "N/A", lat, lng, "");
                        } else {
                            nuevo= new POI(datos.getJSONObject(i).getString("name"),
                                    datos.getJSONObject(i).getString("rating"), lat, lng, "");

                        }
                        //Comprobamos que exista el campo abierto ahora y si hay lo metemos.
                        if (datos.getJSONObject(i).has("opening_hours")) {
                            if (datos.getJSONObject(i).getJSONObject("opening_hours").has("open_now")) {
                                abiertoahora = datos.getJSONObject(i).getJSONObject("opening_hours").getString("open_now");
                                Log.d("OPEN NOW", abiertoahora);
                                nuevo.setOpennow(abiertoahora);
                            }
                        }
                        //A la hora de añadir comprobamos el estado del switch de mostrar solo sitios abiertos y solo metemos los que tengan el opennow a true
                        if (swsitios){
                            if (nuevo.getOpennow().equals("true")){
                                listaPOIs.add(nuevo);
                            }
                        } else {
                            listaPOIs.add(nuevo);
                        }



                    }



                      //  texto = texto + "Nombre: " + datos.getJSONObject(i).getString("name") + " REF: "+ ref.getJSONObject(0).getString()+ "\n";

                    // Actualizamos los datos del adaptador
                    mAdapter.cambioDatos(listaPOIs);
                    //Quitamos el pbar.
                    pbar.setVisibility(View.INVISIBLE);
                    //Si no hay resultados mostramos el tv que informa de que no hay resultados
                    if (listaPOIs.size()==0){
                        tvnodata.setVisibility(View.VISIBLE);
                    }
                    Log.i("CREATE LISTA POIS", texto);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
             }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("CREATEPOILISt","error");

            }
        });
        requestQueue.add(jsonObjectRequest);




    }
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position,
                            long id) {
        POI POI = (POI) parent.getItemAtPosition(position);
        mCallback.onPOISelected(POI);
    }

    @Override
    public void onPause() {
        super.onPause();
        //saveList();

    }
    public boolean compruebaFoto(JSONArray jOb){
        try {
            if (jOb.getJSONObject(0).has("photo_reference")) return true;
            else return false;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return false;
    }



}