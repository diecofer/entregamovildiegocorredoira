package diecofer.uniovi.es.buscadorpoi;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import com.google.android.gms.maps.OnMapReadyCallback;

/**
 * Created by Diecofer on 05/05/2017.
 */

import android.support.v4.app.FragmentActivity;
import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    public static final String longitud = "longitud";
    public static final String latitud = "latitud";
    public static final String name = "nombre";
    public static final String abierto="abierto";
    public static final String rating="rating";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        Intent intent = getIntent();
        String lat= intent.getStringExtra(latitud);
        String lng = intent.getStringExtra(longitud);
        String nombre = intent.getStringExtra(name);
        String rat = intent.getStringExtra(rating);
        String opnw = intent.getStringExtra(abierto);

        // Creacion del texto que va a aparecer en el snippet con la informacion adicional sobre el POI.
        //Si existe rating lo metemos
        String textoadicional ="";
        if (!rating.equals("")){
            textoadicional = textoadicional +"Rating : " + rat;
        }
        //Si existe informacion sobre si esta no abierto lo metemos tambien
        if (!opnw.equals("")){
            //Comprobamos en que estado está
            if (opnw.equals("true")){
                textoadicional= textoadicional + "    Abierto ahora(Open) \n";
            }
            if (opnw.equals("false")){
                textoadicional = textoadicional + "    Cerrado ahora(Closed) \n";
            }
        }

        LatLng home = new LatLng(Double.parseDouble(lat),Double.parseDouble(lng));
        MarkerOptions markerop = new MarkerOptions().position(home).title(nombre);
        markerop.snippet(textoadicional);
        mMap.addMarker(markerop);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(home,18));

    }
}
