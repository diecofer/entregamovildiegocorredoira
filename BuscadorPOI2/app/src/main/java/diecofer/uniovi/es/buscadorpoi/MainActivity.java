package diecofer.uniovi.es.buscadorpoi;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Adapter;
import android.widget.Button;
import android.widget.TextView;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    public static final String PREFERENCES = "preferences";
    public static final String DISTANCIA = "distancia";
    public static final String RADIO = "radio";
    private SharedPreferences prefs = null;
    public static final String SITIOSABIERTOS ="sitios";


    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.index);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        //Si es la primera vez que el usuario entra en la aplicacion establecemos unas preferencias por defecto.
        SharedPreferences prefs = getSharedPreferences(PREFERENCES,Context.MODE_PRIVATE);
        if (prefs.getString(RADIO,null)==null){
            final SharedPreferences.Editor prefsEditor = prefs.edit();
            prefsEditor.putBoolean(DISTANCIA,false);
            prefsEditor.putString(RADIO,String.valueOf(500));
            prefsEditor.putBoolean(SITIOSABIERTOS,false);
            prefsEditor.commit();
        }
    }


    @Override
    public boolean onCreateOptionsMenu (Menu menu){
        //Inflamos el menu
        getMenuInflater().inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    //Gestion de las acciones del menu
    @Override
    public boolean onOptionsItemSelected (MenuItem item){
       int id= item.getItemId();
        if (id == R.id.settings){
            Intent intent = new Intent(this,SettingsActivity.class);
            startActivity(intent);
            return true;
        }
        return false;
    }

    public void onPause () {
       super.onPause();
        }


    //Auxiliar para las categorías
    private void setupViewPager(ViewPager viewPager) {
            Adapter adapter = new Adapter(getSupportFragmentManager());
            adapter.addFragment(new CategoriasTileFragment(), "Tile");
            viewPager.setAdapter(adapter);
    }
    //Adaptador para meter los tiles
    static class Adapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public Adapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

}









