package diecofer.uniovi.es.buscadorpoi;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class POIAdapter extends BaseAdapter {

	static class ViewHolder {
		public TextView mPOIname;
		public TextView mRating;
	}



	public void cambioDatos(ArrayList<POI> listaPOIs){
		this.mPOIs=listaPOIs;
		notifyDataSetChanged();
	}


	private  ArrayList<POI> mPOIs;
	public LayoutInflater mInflater;

	public POIAdapter(Context context, ArrayList<POI> POIs) {

		if (context == null ) {
			throw new IllegalArgumentException();
		}
			
		this.mPOIs = POIs;
		this.mInflater = LayoutInflater.from(context);
	}
		
	@Override
	public int getCount() {

		return mPOIs.size();
	}

	@Override
	public Object getItem(int position) {
		return mPOIs.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View rowView = convertView;
		ViewHolder viewHolder;
		if (rowView == null) {
			rowView = mInflater.inflate(R.layout.list_item_poi, parent, false);
			viewHolder = new ViewHolder();
			viewHolder.mPOIname = (TextView) rowView.findViewById(R.id.nameTextView);
			viewHolder.mRating = (TextView) rowView.findViewById(R.id.ratingTextView);
			rowView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) rowView.getTag();
		}
		POI POI = (POI) getItem(position);
		viewHolder.mPOIname.setText(POI.getName());
		viewHolder.mRating.setText("Rating : "+POI.getRating());
		
		return rowView;
	}
	

}
