package diecofer.uniovi.es.buscadorpoi;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;

/**
 * Created by Diecofer on 26/04/2017.
 */

public class POIListActivity extends AppCompatActivity implements POIListFragment.Callbacks, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    private POIAdapter mAdapter = null;
    public static final String TIPO = "food";
    public GoogleApiClient mGoogleApiClient = null;
    public double latmovil;
    public double longmovil;
    public static final String PREFERENCES = "preferences";
    public static final String DISTANCIA = "distancia";
    public static final String RADIO = "radio";
    private SharedPreferences prefs = null;
    public static final String ABIERTO = "abierto";
    public static final String RATING = "rating";
    public static final String SITIOSABIERTOS ="sitios";


    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.poi_list_single_pane);
        //Creamos una nueva instancia de la api de google
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }
        //Si no hay errores al crear la instancia conectamos
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
        //Si hay errores sacamos un toast informativo
        else {
            Toast.makeText(this, "No se ha podido conectar con la API de Google", Toast.LENGTH_LONG).show();
        }
    }

    /*
    Al clickear sobre un POI mostramos la localizacion de este y varios detalles del mismo
     */
    @Override
    public void onPOISelected(POI POI) {
        Intent intent = new Intent(this, MapsActivity.class);
        intent.putExtra(MapsActivity.latitud, POI.getLatitud());
        intent.putExtra(MapsActivity.longitud, POI.getLongitud());
        intent.putExtra(MapsActivity.name, POI.getName());
        intent.putExtra(MapsActivity.rating , POI.getRating());
        intent.putExtra(MapsActivity.abierto , POI.getOpennow());
        startActivity(intent);
    }
    /*
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }
*/
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
        }
        Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);
        if (mLastLocation != null) {
            latmovil=mLastLocation.getLatitude();
            longmovil=mLastLocation.getLongitude();
        }

        Intent intent = getIntent();
        String tipo = intent.getStringExtra(TIPO);

        prefs = getSharedPreferences(PREFERENCES,Context.MODE_PRIVATE);
        String radio = prefs.getString(RADIO,null);
        Boolean swcheck = prefs.getBoolean(DISTANCIA,false);
        Boolean swsitiosabiertos = prefs.getBoolean(SITIOSABIERTOS,false);

        //Lanzamos el fragmento
        // Crear el fragmento pasándole el parámetro
        POIListFragment fragment =
                POIListFragment.newInstance(tipo,latmovil,longmovil,radio,swcheck,swsitiosabiertos);

        // Añadir el fragmento al contenedor 'fragment_container'
        getSupportFragmentManager().beginTransaction()
                .add(R.id.fragment_container_POI, fragment).commit();

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    Log.d("Entro","Entro");
        //En este punto no se tienen los servicios de google play ( hay que mostrar la localizacion predefinida por el usuario en caso de que no se
        //tenga conexion.
    }
}
