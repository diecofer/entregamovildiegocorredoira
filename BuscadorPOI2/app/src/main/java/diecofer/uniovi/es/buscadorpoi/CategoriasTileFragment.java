/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package diecofer.uniovi.es.buscadorpoi;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class CategoriasTileFragment extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        RecyclerView recyclerView = (RecyclerView) inflater.inflate(
                R.layout.reclycler_view, container, false);
        ContentAdapter adapter = new ContentAdapter(recyclerView.getContext());
        recyclerView.setAdapter(adapter);
        recyclerView.setHasFixedSize(true);
        // Set padding for Tiles
        int tilePadding = getResources().getDimensionPixelSize(R.dimen.tile_padding);
        recyclerView.setPadding(tilePadding, tilePadding, tilePadding, tilePadding);
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        return recyclerView;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView picture;
        public TextView name;
        public ViewHolder(LayoutInflater inflater, ViewGroup parent) {
            super(inflater.inflate(R.layout.item_categoria, parent, false));
            picture = (ImageView) itemView.findViewById(R.id.tile_picture);
            name = (TextView) itemView.findViewById(R.id.tile_title);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Context context = v.getContext();
                    Intent intent = new Intent(context, POIListActivity.class);
                    if (name.getText().equals("Restaurantes") || name.getText().equals("Restaurants")){
                        intent.putExtra(POIListActivity.TIPO, "restaurant");
                        context.startActivity(intent);
                    }
                    if (name.getText().equals("Bares")|| name.getText().equals("Bar")){
                        intent.putExtra(POIListActivity.TIPO, "bar");
                        context.startActivity(intent);
                    }
                    if (name.getText().equals("Cafeterías")|| name.getText().equals("Cafe")){
                        intent.putExtra(POIListActivity.TIPO, "cafe");
                        context.startActivity(intent);
                    }
                    if (name.getText().equals("Museos")|| name.getText().equals("Museum")){
                        intent.putExtra(POIListActivity.TIPO, "museum");
                        context.startActivity(intent);
                    }
                    if (name.getText().equals("Librerías")|| name.getText().equals("Book shop")){
                        intent.putExtra(POIListActivity.TIPO, "book_store");
                        context.startActivity(intent);
                    }
                    if (name.getText().equals("Cajeros")|| name.getText().equals("Atm")){
                        intent.putExtra(POIListActivity.TIPO, "atm");
                        context.startActivity(intent);
                    }
                    if (name.getText().equals("Bibliotecas")|| name.getText().equals("Library")){
                        intent.putExtra(POIListActivity.TIPO, "library");
                        context.startActivity(intent);
                    }
                    if (name.getText().equals("Iglesias")|| name.getText().equals("Church")){
                        intent.putExtra(POIListActivity.TIPO, "church");
                        context.startActivity(intent);
                    }
                    if (name.getText().equals("Farmacias")|| name.getText().equals("Pharmacy")){
                        intent.putExtra(POIListActivity.TIPO, "pharmacy");
                        context.startActivity(intent);
                    }
                    if (name.getText().equals("Aparcamientos")|| name.getText().equals("Parkings")){
                        intent.putExtra(POIListActivity.TIPO, "parking");
                        context.startActivity(intent);
                    }

                }
            });
        }
    }
    /*
     * Mostramos el recycler con un adaptador
     */
    public static class ContentAdapter extends RecyclerView.Adapter<ViewHolder> {
        // Set numbers of List in RecyclerView.
        private static final int LENGTH = 10;
        private final String[] mCategorias;
        private final Drawable[] mFotosCategorias;
        public ContentAdapter(Context context) {
            Resources resources = context.getResources();
            mCategorias = resources.getStringArray(R.array.categorias);
            TypedArray a = resources.obtainTypedArray(R.array.fotos_categorias);
            mFotosCategorias = new Drawable[a.length()];
            for (int i = 0; i < mFotosCategorias.length; i++) {
                mFotosCategorias[i] = a.getDrawable(i);
            }
            a.recycle();
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new ViewHolder(LayoutInflater.from(parent.getContext()), parent);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            holder.picture.setImageDrawable(mFotosCategorias[position % mFotosCategorias.length]);
            holder.name.setText(mCategorias[position % mCategorias.length]);
        }

        @Override
        public int getItemCount() {
            return LENGTH;
        }
    }
}

